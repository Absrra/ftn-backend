/**
 * Company.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {
  
  attributes: {
    rut: {
      type: 'string',
      unique: true
    },
    name: {
      type: 'string'
    },
    logo: {
      type: 'string'
    },
    country: {
      type: 'string'
    },
    currency: {
      type: 'string'
    },
    email: {
      type: 'string'
    },
    agent: {
      type: 'string'
    },
    phone: {
      type: 'array'
    },
    social_net: {
      type: 'array'
    },
    address: {
      type: 'string'
    },
    toJSON: function() {
      let obj = this.toObject();
      return obj;
    }
  }
};

